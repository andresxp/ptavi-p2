#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

fich = open(sys.argv[1], "r")
Lista_completa = fich.readlines()
objeto = calcoohija.CalculadoraHija()

for Linea in Lista_completa:
    final_linea = False
    posicion_letra_encontrada = 0
    Lista_linea = []
    n = 0
    while not final_linea:
        try:
            if len(Linea) == posicion_letra_encontrada:
                if Linea[posicion_letra_encontrada-1] == "\n":
                    numero = Linea[n:posicion_letra_encontrada-1]
                    Lista_linea.append(int(numero))
                else:
                    numero = Linea[n:]
                    Lista_linea.append(int(numero))
                final_linea = True
            elif Linea[posicion_letra_encontrada] == ",":
                if len(Lista_linea) == 0:
                    operador = Linea[n:posicion_letra_encontrada]
                    Lista_linea.append(operador)
                else:
                    numero = Linea[n:posicion_letra_encontrada]
                    Lista_linea.append(int(numero))
                n = posicion_letra_encontrada + 1

            posicion_letra_encontrada = posicion_letra_encontrada + 1
        except ValueError:
            sys.exit("Error: Non numerical parameters")
    n = 1
    if Lista_linea[0] == "suma":
        resultado = objeto.suma(Lista_linea[1], Lista_linea[2])
        while not len(Lista_linea)-2 == n:
            resultado = objeto.suma(resultado, Lista_linea[n+2])
            n = n + 1
        print("Resultado suma " + str(resultado))
    if Lista_linea[0] == "resta":
        resultado = objeto.resta(Lista_linea[1], Lista_linea[2])
        while not len(Lista_linea)-2 == n:
            resultado = objeto.resta(resultado, Lista_linea[n+2])
            n = n + 1
        print("Resultado resta " + str(resultado))
    if Lista_linea[0] == "multiplica":
        resultado = objeto.multiplica(Lista_linea[1], Lista_linea[2])
        while not len(Lista_linea)-2 == n:
            resultado = objeto.multiplica(resultado, Lista_linea[n+2])
            n = n + 1
        print("Resultado multiplica " + str(resultado))
    if Lista_linea[0] == "divide":
        resultado = objeto.divide(Lista_linea[1], Lista_linea[2])
        while not len(Lista_linea)-2 == n:
            resultado = objeto.divide(resultado, Lista_linea[n+2])
            n = n + 1
        print("Resultado divide " + str(resultado))
fich.close()
