#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija
import csv

objeto = calcoohija.CalculadoraHija()

with open(sys.argv[1], newline='') as File:
    reader = csv.reader(File)
    for row in reader:
        n = 1
        try:
            if row[0] == "suma":
                resultado = objeto.suma(int(row[1]), int(row[2]))
                while not len(row)-2 == n:
                    resultado = objeto.suma(resultado, int(row[n+2]))
                    n = n + 1
                print("Resultado suma " + str(resultado))
            if row[0] == "resta":
                resultado = objeto.resta(int(row[1]), int(row[2]))
                while not len(row)-2 == n:
                    resultado = objeto.resta(resultado, int(row[n+2]))
                    n = n + 1
                print("Resultado resta " + str(resultado))
            if row[0] == "multiplica":
                resultado = objeto.multiplica(int(row[1]), int(row[2]))
                while not len(row)-2 == n:
                    resultado = objeto.multiplica(resultado, int(row[n+2]))
                    n = n + 1
                print("Resultado multiplica " + str(resultado))
            if row[0] == "divide":
                resultado = objeto.divide(int(row[1]), int(row[2]))
                while not len(row)-2 == n:
                    resultado = objeto.divide(resultado, int(row[n+2]))
                    n = n + 1
                print("Resultado divide " + str(resultado))
        except ValueError:
            sys.exit("Error: Non numerical parameters")
