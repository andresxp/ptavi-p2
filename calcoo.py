#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():
    def suma(self, valor1, valor2):
        return valor1 + valor2

    def resta(self, valor1, valor2):
        return valor1 - valor2
if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    objeto = Calculadora()
    if sys.argv[2] == "suma":
        resultado = objeto.suma(operando1, operando2)
    elif sys.argv[2] == "resta":
        resultado = objeto.resta(operando1, operando2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
    print(resultado)
