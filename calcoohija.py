#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    def multiplica(self, valor1, valor2):
        return valor1 * valor2

    def divide(self, valor1, valor2):
        try:
            return valor1 / valor2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")
if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    objeto = CalculadoraHija()
    if sys.argv[2] == "suma":
        resultado = objeto.suma(operando1, operando2)
    elif sys.argv[2] == "resta":
        resultado = objeto.resta(operando1, operando2)
    elif sys.argv[2] == "multiplica":
        resultado = objeto.multiplica(operando1, operando2)
    elif sys.argv[2] == "divide":
        resultado = objeto.divide(operando1, operando2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
    print(resultado)
